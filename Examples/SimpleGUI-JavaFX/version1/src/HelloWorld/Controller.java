package HelloWorld;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.event.ActionEvent;

public class Controller {

    @FXML TextField personName;
    @FXML Button helloButton;
    @FXML Label helloMessage;

    public void sayHelloWorld(ActionEvent actionEvent )
    {
        helloMessage.setText("");

        String name = personName.getText().trim();
        if (name.isEmpty())
            return;

        // Check for a "last name, first name" format and switch if present
        int commaIndex = name.indexOf(",");
        if (commaIndex>=0)
        {
            String lastName = name.substring(0, commaIndex).trim();
            String firstName = name.substring(commaIndex+1).trim();
            name = firstName + " " + lastName;
        }

        if (!name.isEmpty())
            helloMessage.setText("Hello, " + name + "!");
    }
}
