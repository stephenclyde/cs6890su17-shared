package HelloWorld;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.event.ActionEvent;

public class Controller {

    @FXML TextField personName;
    @FXML Button helloButton;
    @FXML Label helloMessage;

    public void sayHelloWorld(ActionEvent actionEvent )
    {
        helloMessage.setText("");

        NameFormatter formatter = new NameFormatter();
        String name = formatter.Format(personName.getText());

        if (!name.isEmpty())
            helloMessage.setText("Hello, " + name + "!");
    }
}
