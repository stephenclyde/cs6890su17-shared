package HelloWorld;

/**
 * Created by swc on 7/10/17.
 */
public class NameFormatter {
    public String Format(String personName)
    {
        String name = personName.trim();
        if (name.isEmpty())
            return name;

        // Check for a "last name, first name" format and switch if present
        int commaIndex = name.indexOf(",");
        if (commaIndex>=0)
        {
            String lastName = name.substring(0, commaIndex).trim();
            String firstName = name.substring(commaIndex+1).trim();
            name = firstName + " " + lastName;
        }

        return name;
    }
}
