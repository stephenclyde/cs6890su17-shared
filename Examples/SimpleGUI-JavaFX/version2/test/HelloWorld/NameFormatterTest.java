package HelloWorld;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by swc on 7/10/17.
 */
public class NameFormatterTest {
    @Test
    public void format() throws Exception {
        NameFormatter formatter = new NameFormatter();

        String name = formatter.Format("");
        assertEquals("", name);

        name = formatter.Format("    ");
        assertEquals("", name);

        name = formatter.Format("Joe");
        assertEquals("Joe", name);

        name = formatter.Format("Joe Jones");
        assertEquals("Joe Jones", name);

        name = formatter.Format("Jones, Joe");
        assertEquals("Joe Jones", name);

        name = formatter.Format("    Jones   ,   Joe   ");
        assertEquals("Joe Jones", name);

        name = formatter.Format("    Jones   ,   Joe James  ");
        assertEquals("Joe James Jones", name);

    }

}