/*
 * Adapted from sample test application launcher included with TestFX
 */
package HelloWorld;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit.ApplicationTest;
import org.testfx.api.FxRobot;

import static org.testfx.api.FxAssert.verifyThat;
import static org.testfx.matcher.base.NodeMatchers.hasText;

public class HelloWorldTest extends FxRobot {

    //---------------------------------------------------------------------------------------------
    // FIXTURES.
    //---------------------------------------------------------------------------------------------

    public static class TestApplication extends Application {
        @Override
        public void start(Stage primaryStage) throws Exception {
            Parent root = FXMLLoader.load(getClass().getResource("HelloWorld.fxml"));
            primaryStage.setTitle("Hello World");
            primaryStage.setScene(new Scene(root, 400, 120));
            primaryStage.show();
        }
    }

    //---------------------------------------------------------------------------------------------
    // FIXTURE METHODS.
    //---------------------------------------------------------------------------------------------

    @Before
    public void setup() throws Exception {
        ApplicationTest.launch(TestApplication.class);
    }

    @After
    public void cleanup() throws Exception {
        FxToolkit.cleanupStages();
    }

    //---------------------------------------------------------------------------------------------
    // FEATURE METHODS.
    //---------------------------------------------------------------------------------------------

    @Test
    public void verify_button_test() {
        // expect:
        verifyThat("#personName", (TextField name) -> name.getText().isEmpty());
        verifyThat(".button", hasText("Say Hello"));
        verifyThat("#helloMessage", (Label msg) -> msg.getText().isEmpty());
    }

    @Test
    public void verify_button_behavior() {

        TextField name = lookup("#personName").query();

        // Test Case 1 - Simple first name
        // given:
        name.setText("Joe");
        // when:
        clickOn("#helloButton");
        // then:
        verifyThat("#helloMessage", hasText("Hello, Joe!"));

        // Test Case 2 - Empty string
        // given:
        name.setText("");
        // when:
        clickOn("#helloButton");
        // then:
        verifyThat("#helloMessage", hasText(""));

    }

}
