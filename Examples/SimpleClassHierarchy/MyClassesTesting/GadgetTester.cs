﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;

using MyClasses;

namespace MyClassesTesting
{
    [TestClass]
    public class GadgetTester
    {
        [TestMethod]
        public void Gadget_ShippingCost_01()
        {
            Gadget g = new Gadget() { Length = 0, Width = 0 };
            double shipping = g.ShippingCost;
            Assert.AreEqual(0, shipping);

            g = new Gadget() { Length = 2, Width = 0 };
            shipping = g.ShippingCost;
            Assert.AreEqual(0, shipping);

            g = new Gadget() { Length = 0, Width = 4 };
            shipping = g.ShippingCost;
            Assert.AreEqual(0, shipping);
        }

        [TestMethod]
        public void Gadget_ShippingCost_02()
        {
            Gadget g = new Gadget() { Length = 5, Width = 20 };
            double shipping = g.ShippingCost;
            Assert.AreEqual(6, shipping);
        }

    }
}
