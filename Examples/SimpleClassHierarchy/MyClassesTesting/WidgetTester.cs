﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using MyClasses;

namespace MyClassesTesting
{
    [TestClass]
    public class WidgetTester
    {
        [TestMethod]
        public void WidgetTester_ShippingCost_01()
        {
            // Test shipping cost when the weight is 0
            Widget widget = new Widget() { Weight = 0 };
            double shipping = widget.ShippingCost;
            Assert.AreEqual(0, shipping);
        }

        [TestMethod]
        public void WidgetTester_ShippingCost_02()
        {
            // Test shipping cost when the weight is >0 but <=100
            Widget widget = new Widget() { Weight = 0.01 };
            double shipping = widget.ShippingCost;
            Assert.AreEqual(0.02, shipping);

            widget = new Widget() { Weight = 40 };
            shipping = widget.ShippingCost;
            Assert.AreEqual(0.02, shipping);

            widget = new Widget() { Weight = 100 };
            shipping = widget.ShippingCost;
            Assert.AreEqual(0.02, shipping);

        }

        [TestMethod]
        public void WidgetTester_ShippingCost_03()
        {
            // Test shipping cost when the weight is >100 but <=200

            Widget widget = new Widget() { Weight = 100.01 };
            double shipping = widget.ShippingCost;
            Assert.AreEqual(0.05, shipping);

            widget = new Widget() { Weight = 165 };
            shipping = widget.ShippingCost;
            Assert.AreEqual(0.05, shipping);

            widget = new Widget() { Weight = 200 };
            shipping = widget.ShippingCost;
            Assert.AreEqual(0.05, shipping);
        }

        [TestMethod]
        public void WidgetTester_ShippingCost_04()
        {
            // Test shipping cost when the weight is >200

            Widget widget = new Widget() { Weight = 200.01 };
            double shipping = widget.ShippingCost;
            Assert.AreEqual(200.01*1.5, shipping);
        }


    }
}
