﻿using System;
using System.Runtime.Serialization;

namespace MyClasses
{
    [DataContract]
    public abstract class ThingABob
    {
        private double _unitCost;

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public virtual double UnitCost
        {
            get { return _unitCost; }
            set { _unitCost = Math.Max(0, value); }
        }

        [DataMember]
        public double Markup { get; set;}

        public abstract double ShippingCost { get; }

        public double Price
        {
            get { return UnitCost * Markup + ShippingCost; }
        }

    }
}
