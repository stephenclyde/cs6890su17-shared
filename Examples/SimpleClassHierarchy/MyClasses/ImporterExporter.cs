﻿using System.Collections.Generic;
using System.IO;

namespace MyClasses
{
    public abstract class ImporterExporter
    {
        protected StreamWriter Writer;
        protected StreamReader Reader;

        public void Write(List<ThingABob> list, string filename)
        {
            if (OpenWriter(FullFilename(filename)))
            {
                WriteList(list);
                Writer.Close();            

            }
        }

        public List<ThingABob> Read(string filename)
        {
            List<ThingABob> result = null;
            if (OpenRead(FullFilename(filename)))
            {
                result = ReadList();
                Reader.Close();
            }
            return result;
        }

        protected abstract string FullFilename(string filename); 
        protected abstract void WriteList(List<ThingABob> list);

        protected virtual bool OpenWriter(string filename)
        {
            bool result = false;
            try
            {
                Writer = new StreamWriter(filename);
                result = true;
            }
            catch
            {
                // ignored
            }

            return result;
        }

        protected virtual bool OpenRead(string filename)
        {
            bool result = false;
            try
            {
                Reader = new StreamReader(filename);
                result = true;
            }
            catch
            {
                // ignore
            }

            return result;
        }

        protected abstract List<ThingABob> ReadList();


    }
}
