﻿using System.Runtime.Serialization;

namespace MyClasses
{
    [DataContract]
    public class Widget : ThingABob
    {
        [DataMember]
        public double Weight { get; set; }

        public override double ShippingCost
        {
            get
            {
                double result = 0;
                if (Weight > 200)
                    result = Weight * 1.5;
                else if (Weight > 100)
                    result = .05;
                else if (Weight > 0)
                    result = .02;
                return result;
            }
        }
    }
}
