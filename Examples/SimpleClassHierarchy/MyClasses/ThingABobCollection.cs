﻿using System.Collections.Generic;

namespace MyClasses
{
    public class ThingABobCollection
    {
        private List<ThingABob> _myCollection = new List<ThingABob>();

        public void Add(ThingABob thingy) { _myCollection.Add(thingy); }
        public int Count => _myCollection.Count;
        public ImporterExporter MyImporterExporter { get; set; }

        public void Write(string filename)
        {
            MyImporterExporter?.Write(_myCollection, filename);
        }

        public void Read(string filename)
        {
            if (MyImporterExporter != null)
                _myCollection = MyImporterExporter.Read(filename);
        }

    }
}
