﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Xml.Serialization;

using MyClasses;

namespace TestDataGenerator
{
    public class Program
    {
        private static ImporterExporter[] importerExporters = new ImporterExporter[]
                        {
                            //new JsonImporterExporter(),
                            //new XmlImporterExporter()
                        };

        static void Main(string[] args)
        {
            int selectedImporterExporter = DetermineFileFormat();
            if (selectedImporterExporter > 0)
            {
                // Create a collection, populate with sample data, and save it to a file
                ThingABobCollection collection1 =
                        new ThingABobCollection()
                            { MyImporterExporter = importerExporters[selectedImporterExporter-1] };
                    
                LoadSampleGadgets(collection1);
                Console.WriteLine("Number of objects: {0}", collection1.Count);

                collection1.Write("CollectionData");

                // Create a second collection and load it from the file just save.
                ThingABobCollection collection2 =
                        new ThingABobCollection()
                            { MyImporterExporter = importerExporters[selectedImporterExporter-1] };

                collection2.Read("CollectionData");
                Console.WriteLine("Number of objects: {0}", collection2.Count);
            }
        }

        private static int DetermineFileFormat()
        {
            int result = -1;
            while (result == -1)
            {
                Console.Write("Do you want to work with (1) JSON or (2) XML? ");
                string response = Console.ReadLine();
                response.Trim().ToUpper();
                if (response == "" || response == "EXIT")
                    result = 0;
                else if (response == "1" || response == "JSON" || response == "J")
                    result = 1;
                else if (response == "2" || response == "XML" || response == "X")
                    result = 2;
            }

            return result;       
        }

        private static void LoadSampleGadgets(ThingABobCollection collection)
        {
            collection.Add(new Gadget() { Id = 50, UnitCost = 10.99, Length = 3, Width = 4 });
            collection.Add(new Widget() { Id = 51, Weight = 20.5 });
            collection.Add(new Gadget() { Id = 52, UnitCost = 12.99, Length = 4, Width = 4 });
            collection.Add(new Widget() { Id = 53, Weight = 30.55 });
            collection.Add(new Widget() { Id = 54, Weight = 50.25 });
            collection.Add(new Widget() { Id = 55, Weight = 60.19 });
        }

    }
}
