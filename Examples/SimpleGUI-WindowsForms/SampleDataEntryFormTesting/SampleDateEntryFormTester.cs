﻿using System.Threading;
using System.Windows.Forms;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using SampleDataEntryForm;

//using NUnit.Framework;
using NUnit.Extensions.Forms;

namespace SampleDataEntryFormTesting
{
    [TestClass]
    public class SampleDateEntryFormTester
    {
        [TestMethod]
        public void SampleDateEntryForm_TestEverything()
        {
            ScreeningForm frm = new ScreeningForm();
            frm.Show();

            PrivateObject frmWrapper = new PrivateObject(frm);
            Button saveButton = (Button) frmWrapper.GetField("saveButton");
            TextBox lastName = (TextBox) frmWrapper.GetField("lastName");
            TextBox firstName = (TextBox) frmWrapper.GetField("firstName");
            TextBox heightFeet = (TextBox) frmWrapper.GetField("heightFeet");
            TextBox heightInches = (TextBox) frmWrapper.GetField("heightInches");
            TextBox weight = (TextBox) frmWrapper.GetField("weight");
            TextBox sbp1 = (TextBox) frmWrapper.GetField("sbp1");
            TextBox dbp1 = (TextBox) frmWrapper.GetField("dbp1");
            TextBox sbp2 = (TextBox) frmWrapper.GetField("sbp2");
            TextBox dbp2 = (TextBox) frmWrapper.GetField("dbp2");
            Label averageBp = (Label) frmWrapper.GetField("averageBP");

            Thread.Sleep(100);

            TextBoxTester lastNameTester = new TextBoxTester("lastName", frm);
            TextBoxTester firstNameTester = new TextBoxTester("firstName", frm);
            TextBoxTester heightFeetTester = new TextBoxTester("heightFeet", frm);
            TextBoxTester heightInchesTester = new TextBoxTester("heightInches", frm);
            TextBoxTester weightTester = new TextBoxTester("weight", frm);
            TextBoxTester sbp1Tester = new TextBoxTester("sbp1", frm);
            TextBoxTester dbp1Tester = new TextBoxTester("dbp1", frm);
            TextBoxTester sbp2Tester = new TextBoxTester("sbp2", frm);
            TextBoxTester dbp2Tester = new TextBoxTester("dbp2", frm);
            ButtonTester saveButtonTester = new ButtonTester("saveButton", frm);

            // Make sure the save button not enabled at the beginning
            Assert.IsFalse(saveButton.Enabled);
            Thread.Sleep(100);

            // Check the entry of just a last name.  Save button should still be disabled
            lastNameTester.Enter("Jones");
            Assert.AreEqual("Jones", lastName.Text);
            Assert.IsFalse(saveButton.Enabled);

            // Check the entry of a first name.  Now the save button should be enabled
            firstNameTester.Enter("Joe");
            Assert.AreEqual("Joe", firstName.Text);
            Assert.IsTrue(saveButton.Enabled);

            heightFeetTester.Enter("5");
            Assert.AreEqual("5", heightFeet.Text);
            Assert.IsTrue(saveButton.Enabled);

            heightFeetTester.Enter("");
            Assert.IsFalse(saveButton.Enabled);

            heightFeetTester.Enter("6");
            Assert.AreEqual("6", heightFeet.Text);
            Assert.IsTrue(saveButton.Enabled);

            heightFeetTester.Enter("0");
            Assert.IsFalse(saveButton.Enabled);

            heightFeetTester.Enter("4");
            Assert.AreEqual("4", heightFeet.Text);
            Assert.IsTrue(saveButton.Enabled);

            heightFeetTester.Enter("-1");
            Assert.IsFalse(saveButton.Enabled);

            heightFeetTester.Enter("3");
            Assert.AreEqual("3", heightFeet.Text);
            Assert.IsTrue(saveButton.Enabled);
     
            heightFeetTester.Enter("50");
            Assert.IsFalse(saveButton.Enabled);

            heightFeetTester.Enter("abc");
            Assert.IsFalse(saveButton.Enabled);

            heightFeetTester.Enter("-1");
            Assert.IsFalse(saveButton.Enabled);

            heightFeetTester.Enter("5");
            Assert.AreEqual("5", heightFeet.Text);
            Assert.IsTrue(saveButton.Enabled);

            heightFeetTester.Enter("xyz");
            Assert.IsFalse(saveButton.Enabled);

            heightFeetTester.Enter("0");
            heightInchesTester.Enter("62");
            Assert.IsTrue(saveButton.Enabled);
            Assert.AreEqual("5", heightFeet.Text);
            Assert.AreEqual("2", heightInches.Text);

            weightTester.Enter("132");
            Assert.AreEqual("132", weight.Text);
            Assert.IsTrue(saveButton.Enabled);

            weightTester.Enter("aaa");
            Assert.IsFalse(saveButton.Enabled);

            weightTester.Enter("132");
            Assert.AreEqual("132", weight.Text);
            Assert.IsTrue(saveButton.Enabled);

            weightTester.Enter("abc");
            Assert.IsFalse(saveButton.Enabled);

            weightTester.Enter("132");
            Assert.AreEqual("132", weight.Text);
            Assert.IsTrue(saveButton.Enabled);

            sbp1Tester.Enter("96");
            dbp1Tester.Enter("138");
            Assert.AreEqual("96", sbp1.Text);
            Assert.AreEqual("138", dbp1.Text);
            Assert.IsTrue(saveButton.Enabled);

            sbp2Tester.Enter("98");
            dbp2Tester.Enter("140");
            Assert.AreEqual("98", sbp2.Text);
            Assert.AreEqual("140", dbp2.Text);
            Assert.IsTrue(saveButton.Enabled);

            Assert.AreEqual("97/139", averageBp.Text);


        }
    }
}
