﻿namespace SampleDataEntryForm
{
    partial class ScreeningForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lastNameLabel = new System.Windows.Forms.Label();
            this.lastName = new System.Windows.Forms.TextBox();
            this.firstName = new System.Windows.Forms.TextBox();
            this.firstNameLabel = new System.Windows.Forms.Label();
            this.heightLabel = new System.Windows.Forms.Label();
            this.weightLabel = new System.Windows.Forms.Label();
            this.bp1Label = new System.Windows.Forms.Label();
            this.bp2Label = new System.Windows.Forms.Label();
            this.heightInches = new System.Windows.Forms.TextBox();
            this.feetLabel = new System.Windows.Forms.Label();
            this.inchesLabel = new System.Windows.Forms.Label();
            this.heightFeet = new System.Windows.Forms.TextBox();
            this.weight = new System.Windows.Forms.TextBox();
            this.lbsLabel = new System.Windows.Forms.Label();
            this.sbp1 = new System.Windows.Forms.TextBox();
            this.dbp1 = new System.Windows.Forms.TextBox();
            this.over1Label = new System.Windows.Forms.Label();
            this.over2Label = new System.Windows.Forms.Label();
            this.dbp2 = new System.Windows.Forms.TextBox();
            this.sbp2 = new System.Windows.Forms.TextBox();
            this.averageBPLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.averageBP = new System.Windows.Forms.Label();
            this.saveButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cancelButton = new System.Windows.Forms.Button();
            this.myErrors = new System.Windows.Forms.ErrorProvider(this.components);
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.myErrors)).BeginInit();
            this.SuspendLayout();
            // 
            // lastNameLabel
            // 
            this.lastNameLabel.AutoSize = true;
            this.lastNameLabel.Location = new System.Drawing.Point(23, 21);
            this.lastNameLabel.Name = "lastNameLabel";
            this.lastNameLabel.Size = new System.Drawing.Size(64, 13);
            this.lastNameLabel.TabIndex = 0;
            this.lastNameLabel.Text = "Last Name: ";
            // 
            // lastName
            // 
            this.lastName.Location = new System.Drawing.Point(93, 18);
            this.lastName.Name = "lastName";
            this.lastName.Size = new System.Drawing.Size(191, 20);
            this.lastName.TabIndex = 0;
            this.lastName.Text = "(enter last name here)";
            this.lastName.TextChanged += new System.EventHandler(this.ValidateStuff);
            // 
            // firstName
            // 
            this.firstName.Location = new System.Drawing.Point(93, 44);
            this.firstName.Name = "firstName";
            this.firstName.Size = new System.Drawing.Size(191, 20);
            this.firstName.TabIndex = 1;
            this.firstName.Text = "(enter first name here)";
            this.firstName.TextChanged += new System.EventHandler(this.ValidateStuff);
            // 
            // firstNameLabel
            // 
            this.firstNameLabel.AutoSize = true;
            this.firstNameLabel.Location = new System.Drawing.Point(23, 47);
            this.firstNameLabel.Name = "firstNameLabel";
            this.firstNameLabel.Size = new System.Drawing.Size(63, 13);
            this.firstNameLabel.TabIndex = 2;
            this.firstNameLabel.Text = "First Name: ";
            // 
            // heightLabel
            // 
            this.heightLabel.AutoSize = true;
            this.heightLabel.Location = new System.Drawing.Point(19, 15);
            this.heightLabel.Name = "heightLabel";
            this.heightLabel.Size = new System.Drawing.Size(44, 13);
            this.heightLabel.TabIndex = 4;
            this.heightLabel.Text = "Height: ";
            // 
            // weightLabel
            // 
            this.weightLabel.AutoSize = true;
            this.weightLabel.Location = new System.Drawing.Point(20, 41);
            this.weightLabel.Name = "weightLabel";
            this.weightLabel.Size = new System.Drawing.Size(47, 13);
            this.weightLabel.TabIndex = 5;
            this.weightLabel.Text = "Weight: ";
            // 
            // bp1Label
            // 
            this.bp1Label.AutoSize = true;
            this.bp1Label.Location = new System.Drawing.Point(20, 15);
            this.bp1Label.Name = "bp1Label";
            this.bp1Label.Size = new System.Drawing.Size(83, 13);
            this.bp1Label.TabIndex = 6;
            this.bp1Label.Text = "BP Reading #1:";
            // 
            // bp2Label
            // 
            this.bp2Label.AutoSize = true;
            this.bp2Label.Location = new System.Drawing.Point(20, 46);
            this.bp2Label.Name = "bp2Label";
            this.bp2Label.Size = new System.Drawing.Size(83, 13);
            this.bp2Label.TabIndex = 7;
            this.bp2Label.Text = "BP Reading #2:";
            // 
            // heightInches
            // 
            this.heightInches.Location = new System.Drawing.Point(163, 12);
            this.heightInches.Name = "heightInches";
            this.heightInches.Size = new System.Drawing.Size(32, 20);
            this.heightInches.TabIndex = 1;
            this.heightInches.Text = "0";
            this.heightInches.Leave += new System.EventHandler(this.ValidateStuff);
            // 
            // feetLabel
            // 
            this.feetLabel.AutoSize = true;
            this.feetLabel.Location = new System.Drawing.Point(126, 15);
            this.feetLabel.Name = "feetLabel";
            this.feetLabel.Size = new System.Drawing.Size(25, 13);
            this.feetLabel.TabIndex = 10;
            this.feetLabel.Text = "feet";
            // 
            // inchesLabel
            // 
            this.inchesLabel.AutoSize = true;
            this.inchesLabel.Location = new System.Drawing.Point(198, 15);
            this.inchesLabel.Name = "inchesLabel";
            this.inchesLabel.Size = new System.Drawing.Size(38, 13);
            this.inchesLabel.TabIndex = 11;
            this.inchesLabel.Text = "inches";
            // 
            // heightFeet
            // 
            this.heightFeet.Location = new System.Drawing.Point(91, 12);
            this.heightFeet.Name = "heightFeet";
            this.heightFeet.Size = new System.Drawing.Size(32, 20);
            this.heightFeet.TabIndex = 0;
            this.heightFeet.Text = "6";
            this.heightFeet.Leave += new System.EventHandler(this.ValidateStuff);
            // 
            // weight
            // 
            this.weight.Location = new System.Drawing.Point(91, 38);
            this.weight.Name = "weight";
            this.weight.Size = new System.Drawing.Size(32, 20);
            this.weight.TabIndex = 2;
            this.weight.Text = "195";
            this.weight.Leave += new System.EventHandler(this.ValidateStuff);
            // 
            // lbsLabel
            // 
            this.lbsLabel.AutoSize = true;
            this.lbsLabel.Location = new System.Drawing.Point(126, 41);
            this.lbsLabel.Name = "lbsLabel";
            this.lbsLabel.Size = new System.Drawing.Size(20, 13);
            this.lbsLabel.TabIndex = 13;
            this.lbsLabel.Text = "lbs";
            // 
            // sbp1
            // 
            this.sbp1.Location = new System.Drawing.Point(127, 12);
            this.sbp1.Name = "sbp1";
            this.sbp1.Size = new System.Drawing.Size(32, 20);
            this.sbp1.TabIndex = 0;
            this.sbp1.Text = "90";
            this.sbp1.Leave += new System.EventHandler(this.ValidateStuff);
            // 
            // dbp1
            // 
            this.dbp1.Location = new System.Drawing.Point(199, 12);
            this.dbp1.Name = "dbp1";
            this.dbp1.Size = new System.Drawing.Size(32, 20);
            this.dbp1.TabIndex = 1;
            this.dbp1.Text = "124";
            this.dbp1.Leave += new System.EventHandler(this.ValidateStuff);
            // 
            // over1Label
            // 
            this.over1Label.AutoSize = true;
            this.over1Label.Location = new System.Drawing.Point(165, 15);
            this.over1Label.Name = "over1Label";
            this.over1Label.Size = new System.Drawing.Size(28, 13);
            this.over1Label.TabIndex = 16;
            this.over1Label.Text = "over";
            // 
            // over2Label
            // 
            this.over2Label.AutoSize = true;
            this.over2Label.Location = new System.Drawing.Point(165, 46);
            this.over2Label.Name = "over2Label";
            this.over2Label.Size = new System.Drawing.Size(28, 13);
            this.over2Label.TabIndex = 19;
            this.over2Label.Text = "over";
            // 
            // dbp2
            // 
            this.dbp2.Location = new System.Drawing.Point(199, 43);
            this.dbp2.Name = "dbp2";
            this.dbp2.Size = new System.Drawing.Size(32, 20);
            this.dbp2.TabIndex = 3;
            this.dbp2.Text = "120";
            this.dbp2.Leave += new System.EventHandler(this.ValidateStuff);
            // 
            // sbp2
            // 
            this.sbp2.Location = new System.Drawing.Point(127, 43);
            this.sbp2.Name = "sbp2";
            this.sbp2.Size = new System.Drawing.Size(32, 20);
            this.sbp2.TabIndex = 2;
            this.sbp2.Text = "80";
            this.sbp2.Leave += new System.EventHandler(this.ValidateStuff);
            // 
            // averageBPLabel
            // 
            this.averageBPLabel.AutoSize = true;
            this.averageBPLabel.Location = new System.Drawing.Point(20, 71);
            this.averageBPLabel.Name = "averageBPLabel";
            this.averageBPLabel.Size = new System.Drawing.Size(67, 13);
            this.averageBPLabel.TabIndex = 20;
            this.averageBPLabel.Text = "Average BP:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(129, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "0 / 0";
            // 
            // averageBP
            // 
            this.averageBP.AutoSize = true;
            this.averageBP.Location = new System.Drawing.Point(127, 71);
            this.averageBP.Name = "averageBP";
            this.averageBP.Size = new System.Drawing.Size(30, 13);
            this.averageBP.TabIndex = 22;
            this.averageBP.Text = "0 / 0";
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(209, 263);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 3;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.dbp1);
            this.panel1.Controls.Add(this.bp1Label);
            this.panel1.Controls.Add(this.averageBP);
            this.panel1.Controls.Add(this.bp2Label);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.sbp1);
            this.panel1.Controls.Add(this.averageBPLabel);
            this.panel1.Controls.Add(this.over1Label);
            this.panel1.Controls.Add(this.over2Label);
            this.panel1.Controls.Add(this.sbp2);
            this.panel1.Controls.Add(this.dbp2);
            this.panel1.Location = new System.Drawing.Point(26, 152);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(258, 99);
            this.panel1.TabIndex = 24;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.heightFeet);
            this.panel2.Controls.Add(this.heightLabel);
            this.panel2.Controls.Add(this.weightLabel);
            this.panel2.Controls.Add(this.lbsLabel);
            this.panel2.Controls.Add(this.heightInches);
            this.panel2.Controls.Add(this.weight);
            this.panel2.Controls.Add(this.feetLabel);
            this.panel2.Controls.Add(this.inchesLabel);
            this.panel2.Location = new System.Drawing.Point(26, 75);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(257, 72);
            this.panel2.TabIndex = 25;
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(26, 263);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 2;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // myErrors
            // 
            this.myErrors.ContainerControl = this;
            // 
            // ScreeningForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(302, 301);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.firstName);
            this.Controls.Add(this.firstNameLabel);
            this.Controls.Add(this.lastName);
            this.Controls.Add(this.lastNameLabel);
            this.Name = "ScreeningForm";
            this.Text = "Screening Measurements";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.myErrors)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lastNameLabel;
        private System.Windows.Forms.TextBox lastName;
        private System.Windows.Forms.TextBox firstName;
        private System.Windows.Forms.Label firstNameLabel;
        private System.Windows.Forms.Label heightLabel;
        private System.Windows.Forms.Label weightLabel;
        private System.Windows.Forms.Label bp1Label;
        private System.Windows.Forms.Label bp2Label;
        private System.Windows.Forms.TextBox heightInches;
        private System.Windows.Forms.Label feetLabel;
        private System.Windows.Forms.Label inchesLabel;
        private System.Windows.Forms.TextBox heightFeet;
        private System.Windows.Forms.TextBox weight;
        private System.Windows.Forms.Label lbsLabel;
        private System.Windows.Forms.TextBox sbp1;
        private System.Windows.Forms.TextBox dbp1;
        private System.Windows.Forms.Label over1Label;
        private System.Windows.Forms.Label over2Label;
        private System.Windows.Forms.TextBox dbp2;
        private System.Windows.Forms.TextBox sbp2;
        private System.Windows.Forms.Label averageBPLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label averageBP;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.ErrorProvider myErrors;
    }
}

