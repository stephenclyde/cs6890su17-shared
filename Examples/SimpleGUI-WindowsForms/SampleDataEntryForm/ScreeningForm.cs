﻿using System;
using System.Windows.Forms;

namespace SampleDataEntryForm
{
    public partial class ScreeningForm : Form
    {
        public ScreeningForm()
        {
            InitializeComponent();
            saveButton.Enabled = false;    
        }

        private void ValidateStuff(object sender, EventArgs e)
        {
            bool result = true;
            result &= ValidateNames();
            result &= ValidateHeight();
            result &= ValidateWeight();
            result &= ValidateBp1();
            result &= ValidateBp2();

            if (result)
            {
                saveButton.Enabled = true;
                ComputeAverageBp();
            }
            else
                saveButton.Enabled = false;
        }

        private bool ValidateNames()
        {
            bool result = true;
            if (lastName.Text != "")
            {
                myErrors.SetError(lastNameLabel, "");
                if (lastName.Text == @"(enter last name here)")
                    result = false;
            }
            else
            {
                myErrors.SetError(lastNameLabel, @"The last name cannot be blank");
                result = false;
            }

            if (firstName.Text != "")
            {
                myErrors.SetError(firstNameLabel, "");
                if (firstName.Text == @"(enter first name here)")
                    result = false;
            }
            else
            {
                myErrors.SetError(firstNameLabel, "The first name cannot be blank");
                result = false;
            }

            return result;
        }

        private bool ValidateHeight()
        {
            bool result = false;
            try
            {
                if (heightInches.Text == "")
                    heightInches.Text = @"0";
                if (heightFeet.Text == "")
                    heightFeet.Text = @"0";

                Int16 tmp1 = Convert.ToInt16(heightInches.Text);
                Int16 tmp2 = Convert.ToInt16(heightFeet.Text);

                tmp1 += Convert.ToInt16(tmp2 * 12);

                if (tmp1 <= 0 || tmp1 > 120)
                    myErrors.SetError(heightLabel, "The height has to be between 1 and 10 feet");
                else
                {
                    tmp2 = Convert.ToInt16(tmp1 / 12);
                    tmp1 = Convert.ToInt16(tmp1 % 12);
                    heightFeet.Text = tmp2.ToString();
                    heightInches.Text = tmp1.ToString();
                    myErrors.SetError(heightLabel, "");
                    result = true;
                }
            }
            catch
            {
                myErrors.SetError(heightLabel, "Invalid Height");
            }
            return result;
        }

        private bool ValidateWeight()
        {
            bool result = false;
            try
            {
                if (weight.Text == "")
                    weight.Text = @"0";

                Int16 tmp1 = Convert.ToInt16(weight.Text);

                if (tmp1 < 0 || tmp1 > 900)
                    myErrors.SetError(weightLabel, @"The weight has to be between 1 and 900 lbs");
                else
                {
                    myErrors.SetError(weightLabel, "");
                    result = true;
                }
            }
            catch
            {
                myErrors.SetError(weightLabel, @"Invalid Weight");
            }
            return result;
        }

        private bool ValidateBp1()
        {
            bool result = false;
            try
            {
                if (sbp1.Text == "")
                    sbp1.Text = @"0";
                if (dbp1.Text == "")
                    dbp1.Text = @"0";

                Int16 tmp1 = Convert.ToInt16(sbp1.Text);
                Int16 tmp2 = Convert.ToInt16(dbp1.Text);

                if (tmp1 <= 0 || tmp1 > 200 || tmp2<=0 || tmp2>200)
                    myErrors.SetError(bp1Label, "Both measurements have to be between 1 and 200");
                else
                {
                    myErrors.SetError(bp1Label, "");
                    result = true;
                }
            }
            catch
            {
                myErrors.SetError(bp1Label, "Invalid blood pressure measure");
            }
            return result;
        }

        private bool ValidateBp2()
        {
            bool result = false;
            try
            {
                if (sbp2.Text == "")
                    sbp2.Text = @"0";
                if (dbp2.Text == "")
                    dbp2.Text = @"0";

                Int16 tmp1 = Convert.ToInt16(sbp2.Text);
                Int16 tmp2 = Convert.ToInt16(dbp2.Text);

                if (tmp1 <= 0 || tmp1 > 200 || tmp2 <= 0 || tmp2 > 200)
                    myErrors.SetError(bp2Label, "Both measurements have to be between 1 and 200");
                else
                {
                    myErrors.SetError(bp2Label, "");
                    result = true;
                }
            }
            catch
            {
                myErrors.SetError(bp2Label, "Invalid blood pressure measure");
            }
            return result;
        }

        private void ComputeAverageBp()
        {
            try
            {
                Int16 tmpSb1 = Convert.ToInt16(sbp1.Text);
                Int16 tmpDb1 = Convert.ToInt16(dbp1.Text);
                Int16 tmpSb2 = Convert.ToInt16(sbp2.Text);
                Int16 tmpDb2 = Convert.ToInt16(dbp2.Text);

                Int16 aveSb = Convert.ToInt16((tmpSb1 + tmpSb2) / 2);
                Int16 aveDb = Convert.ToInt16((tmpDb1 + tmpDb2) / 2);
                averageBP.Text = aveSb.ToString() + @"/" + aveDb.ToString();
                myErrors.SetError(averageBPLabel, "");
            }
            catch
            {
                myErrors.SetError(averageBPLabel, "Invalid blood pressure measures");
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            // Save logic not actually shown -- not relevant to purpose of example
            Close();
        }

    }

}
