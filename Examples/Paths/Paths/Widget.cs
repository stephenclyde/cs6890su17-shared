﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Paths
{
    public class Widget
    {
        private readonly Random _random = new Random();

        protected int _someInternalState = 1;
        public int InternalState => _someInternalState;

        public enum PaddingType
        {
            Spaces,
            Tabs,
            NewLines
        };

        public PaddingType Padding { get; set; }

        /// <summary>
        /// Sample Funcion #1 - returns either an 11 or 19 depending on in the input parameter x
        /// </summary>
        /// <param name="x">A boolean value that controls the behavior of the function</param>
        /// <returns>If x is true, then 19; otherwise 11</returns>

        public int Function01(bool x)
        {
            var result = 11;
            if (x)
                result = 19;
            return result;
        }

        public int Function01_b(bool x)
        {
            if (x)
                return 19;
            else
                return 11;
        }

        public int Function01_c(bool x)
        {
            return (x) ? 19 : 11;
        }

        /// <summary>
        /// Sample Funcion #2 - returns either an 11 or 19 depending on in the input parameters x and y
        /// </summary>
        /// <param name="x">A boolean value that controls the behavior of the function</param>
        /// <param name="y">Another boolean value that controls the behavior of the function</param>
        /// <returns>Return 19 if both x and y are true; otherwise it returns 11</returns>
        public int Function02(bool x, bool y)
        {
            var result = 11;
            if (x && y)
                result = 19;
            return result;
        }

        /// <summary>
        /// Sample Funcion #3 - returns either an 11 or 19 depending on in the input parameters x, y, and z
        /// </summary>
        /// <param name="x">A boolean value that controls the behavior of the function</param>
        /// <param name="y">A 2nd boolean value that controls the behavior of the function</param>
        /// <param name="z">A 3rd boolean value that controls the behavior of the function</param>
        /// <returns>Return 19 if either x or y are true and z is true; otherwise it return 11</returns>
        public int Function03(bool x, bool y, bool z)
        {
            var result = 11;
            if ((x || y) && z)
                result = 19;
            return result;
        }

        /// <summary>
        /// This function is suppose to count to the non-null and non-blank entries and that don't
        /// equal "BLANK" or "SPACE"
        /// in an array, optionally skipping the first and line entry
        /// 
        /// NOTE: THERE A CURRENTLY DEFECTS IN THIS IMPLEMENTATION AND IT IS COULD
        /// BE SIMPLIER
        /// </summary>
        /// <param name="data"></param>
        /// <param name="skipFirstAndLast"></param>
        /// <returns></returns>
        public int Function04_a(List<string> data, bool skipFirstAndLast)
        {
            var startingIndex = 0;
            if (skipFirstAndLast)
                startingIndex++;

            var count = 0;
            for (var index = startingIndex; index < data.Count; index++)
            {
                if (!string.IsNullOrWhiteSpace(data[index]) &&
                    !(data[index] == "BLNK" || data[index] == "SPACE"))
                    count++;
            }

            if (skipFirstAndLast)
                count--;

            return count;
        }

        public int Function04_b(List<string> data, bool skipFirstAndLast)
        {
            var count = 0;
            var startingIndex = 0;
            var endingIndex = data.Count;
            if (skipFirstAndLast)
            {
                startingIndex++;
                endingIndex--;
            }

            for (var index = startingIndex; index < endingIndex; index++)
            {
                if (!string.IsNullOrWhiteSpace(data[index]) &&
                    !(data[index] == "BLANK" || data[index] == "SPACE"))
                    count++;
            }

            return count;
        }

        public int Function04(List<string> data, bool skipFirstAndLast)
        {
            var count = 0;
            if (data != null && data.Count != 0)
            {
                var startingIndex = 0;
                var endingIndex = data.Count;
                if (skipFirstAndLast)
                {
                    startingIndex++;
                    endingIndex--;
                }

                for (var index = startingIndex; index < endingIndex; index++)
                {
                    if (!string.IsNullOrWhiteSpace(data[index]) &&
                        !(data[index] == "BLANK" || data[index] == "SPACE"))
                        count++;
                }
            }
            return count;
        }

        /// <summary>
        /// Sample Function #5 - reorders the fields of a records
        /// </summary>
        /// <param name="data">a record with 3 comma separated fields</param>
        /// <returns></returns>
        public string Function05(string data)
        {
            string result = null;
            string[] fields = data.Split(',');
            if (fields.Length == 3)
                result = fields[2].Trim() + "," + fields[0].Trim() + "," + fields[1];
            return result;
        }

        /// <summary>
        /// Sample Funcion #6 - computes a values based in the input parameter and an internal state, with
        /// an unpredicable time delay, and sets the internal state.
        /// </summary>
        /// <param name="x">some integer between -100 and 100</param>
        /// <returns>the widget's internal state</returns>
        public int Function06(int x)
        {
            if (x < -100 || x > 100)
                return _someInternalState;

            // Simulate a bunch of computation or I/O
            int msToSleep = _random.Next(100);
            Thread.Sleep(msToSleep);

            if (_someInternalState < -100 || _someInternalState > 100)
                _someInternalState = 1;

            if ((x%4) == 0)
                _someInternalState += x;
            else
                _someInternalState *= 2;

            return _someInternalState;
        }

        /// <summary>
        /// Sample Funcion #7 - generates a string of some number of A's, B's, and C's, with padding in between
        /// The Padding properties specifies the type of padding
        /// 
        /// NOTE: This implementation contains bugs and testing inefficiences
        /// </summary>
        /// <param name="a">Number of A's to include in the string</param>
        /// <param name="b">Number of A's to include in the string</param>
        /// <param name="c">Number of A's to include in the string</param>
        /// <returns></returns>
        public string Function07(int a, int b, int c)
        {
            var result = "";
            result = result.PadRight(result.Length + a, 'A');

            switch (Padding)
            {
                case PaddingType.Spaces:
                    result += " ";
                    break;
                case PaddingType.Tabs:
                    result += "\t";
                    break;
                case PaddingType.NewLines:
                    result += "\n";
                    break;
            }

            result = result.PadRight(result.Length + b, 'B');

            switch (Padding)
            {
                case PaddingType.Spaces:
                    result += " ";
                    break;
                case PaddingType.Tabs:
                    result += "\t";
                    break;
                case PaddingType.NewLines:
                    result += "\n";
                    break;
            }

            result = result.PadRight(result.Length + c, 'C');

            return result;
        }

        /// <summary>
        /// Sample Funcion #8 - returns true if x greater than y and x is divisable by 12345 and x less than 2*y
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bool Function08(int x, int y)
        {
            return x > y && (x%12345)==0 && x < 2*y;
        }
    }
}
