﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paths
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var w = new Widget();

            Console.WriteLine($"w.Function01(true)={w.Function01(true)}");
            Console.WriteLine($"w.Function02(true, false)={w.Function02(true, false)}");
            Console.WriteLine($"w.Function03(true, false, true)={w.Function03(true, false, true)}");


        }
    }
}
