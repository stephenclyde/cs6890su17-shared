﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Paths;

namespace PathsTesting1
{
    [TestClass()]
    public class WidgetTester
    {
        [TestMethod()]
        public void Widget_Function01Test()
        {
            Widget w = new Widget();

            int result = w.Function01(true);
            Assert.AreEqual(19, result);

            //result = w.Function01(false);
            //Assert.AreEqual(11, result);
        }

        [TestMethod()]
        public void Widget_Function01_bTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void Widget_Function01_cTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void Widget_Function02Test()
        {
            Widget w = new Widget();

            int result = w.Function02(true, true);
            Assert.AreEqual(19, result);

            result = w.Function02(true, false);
            Assert.AreEqual(11, result);

            result = w.Function02(false, true);
            Assert.AreEqual(11, result);

            /* redundant
            * result = w.Function02(false, false);
            * Assert.AreEqual(11, result);
            */
        }

        [TestMethod()]
        public void Widget_Function03Test()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void Widget_Function04Test()
        {
            Widget w = new Widget();

            List<string> data = new List<string>();
            Assert.AreEqual(0, w.Function04(data, true));            

            data = new List<string>() { "hello", "BLANK", "joe"};
            Assert.AreEqual(2, w.Function04(data, false));
            Assert.AreEqual(0, w.Function04(data, true));

            data = new List<string>() { "hello", "joe", "BLANK"};
            Assert.AreEqual(2, w.Function04(data, false));

            Assert.AreEqual(1, w.Function04(data, true));

            Assert.AreEqual(0, w.Function04(null, false));

            data = new List<string>() { "hello", "joe", "SPACE", "Mary" };
            Assert.AreEqual(3, w.Function04(data, false));
            Assert.AreEqual(1, w.Function04(data, true));

            data = new List<string>() { "hello", "joe", "SPACE", "Mary,", "SPACE" };
            Assert.AreEqual(3, w.Function04(data, false));
            Assert.AreEqual(2, w.Function04(data, true));

            data = new List<string>() { "SPACE", "hello", "joe", "SPACE", "Mary,", "SPACE" };
            Assert.AreEqual(3, w.Function04(data, false));
            Assert.AreEqual(3, w.Function04(data, true));
        }

        [TestMethod()]
        public void Widget_Function04_bTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void Widget_Function05Test()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void Widget_Function06Test()
        {
            Widget w = new Widget();
            Assert.AreEqual(1, w.InternalState);

            Assert.AreEqual(2, w.Function06(9));
            Assert.AreEqual(2, w.InternalState);

            Assert.AreEqual(82, w.Function06(80));
            Assert.AreEqual(82, w.InternalState);

            Assert.AreEqual(82, w.Function06(101));
            Assert.AreEqual(82, w.InternalState);

            Assert.AreEqual(122, w.Function06(40));
            Assert.AreEqual(122, w.InternalState);

            Assert.AreEqual(2, w.Function06(1));
            Assert.AreEqual(2, w.InternalState);

            Assert.AreEqual(2, w.Function06(-101));
            Assert.AreEqual(2, w.InternalState);

            Assert.AreEqual(-78, w.Function06(-80));
            Assert.AreEqual(-78, w.InternalState);

            Assert.AreEqual(-156, w.Function06(1));
            Assert.AreEqual(-156, w.InternalState);

            Assert.AreEqual(2, w.Function06(1));
            Assert.AreEqual(2, w.InternalState);
        }

        [TestMethod()]
        public void Widget_Function06Test_WithScript()
        {
            Widget w = new Widget();
            Assert.AreEqual(1, w.InternalState);

            List<Function06TestData> suite = new List<Function06TestData>()
            {
                new Function06TestData() {Input = 9, Expected = 2},
                new Function06TestData() {Input = 80, Expected = 82},
                new Function06TestData() {Input = 101, Expected = 82},
                new Function06TestData() {Input = 40, Expected = 122},
                new Function06TestData() {Input = 1, Expected = 2},
                new Function06TestData() {Input = -101, Expected = 2},
                new Function06TestData() {Input = -80, Expected = -78},
                new Function06TestData() {Input = 1, Expected = -156},
                new Function06TestData() {Input = 1, Expected = 2}
            };

            foreach (Function06TestData tc  in suite)
            {
                Assert.AreEqual(tc.Expected, w.Function06(tc.Input));
                Assert.AreEqual(tc.Expected, w.InternalState);
            }
        }

        private class Function06TestData
        {
            public int Input { get; set; }
            public int Expected { get; set; }
        }

        [TestMethod()]
        public void Widget_Function07Test()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void Widget_Function08Test()
        {
            Assert.Fail();
        }
    }

    public class TestableWidget : Widget
    {
        public void SetInternalState(int internalState)
        {
            _someInternalState = internalState;
        }
    }
}